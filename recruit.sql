/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50536
 Source Host           : localhost:3306
 Source Schema         : recruit

 Target Server Type    : MySQL
 Target Server Version : 50536
 File Encoding         : 65001

 Date: 27/12/2021 23:14:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for enterprise
-- ----------------------------
DROP TABLE IF EXISTS `enterprise`;
CREATE TABLE `enterprise`  (
                               `eid` int(11) NOT NULL AUTO_INCREMENT COMMENT '企业用户id（主键）',
                               `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
                               `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
                               `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '电子邮箱',
                               `gmt_create` datetime NOT NULL COMMENT '创建时间',
                               `gmt_modified` datetime NOT NULL COMMENT '修改时间',
                               `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
                               PRIMARY KEY (`eid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of enterprise
-- ----------------------------
INSERT INTO `enterprise` VALUES (1, 'gyb', '123456', '673354892@qq.com', '2021-12-27 14:45:13', '2021-12-27 14:45:13', 0);

-- ----------------------------
-- Table structure for enterprise_display
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_display`;
CREATE TABLE `enterprise_display`  (
                                       `eid` int(11) NOT NULL COMMENT '企业用户id（外键、主键）',
                                       `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '公司简介',
                                       `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公司地址',
                                       `website` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公司官网',
                                       `telephone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系电话',
                                       `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公司名称',
                                       `registration_time` datetime NOT NULL COMMENT '注册时间',
                                       `registered_capital` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '注册资金',
                                       `gmt_create` datetime NOT NULL COMMENT '创建时间',
                                       `gmt_modified` datetime NOT NULL COMMENT '修改时间',
                                       `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
                                       PRIMARY KEY (`eid`) USING BTREE,
                                       CONSTRAINT `eid` FOREIGN KEY (`eid`) REFERENCES `enterprise` (`eid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of enterprise_display
-- ----------------------------
INSERT INTO `enterprise_display` VALUES (1, '好描述', '黑龙江大庆', 'www.baidu.com', '15776545893', '坚球公司', '2021-12-27 06:39:38', '500000', '2021-12-27 14:53:12', '2021-12-27 14:53:12', 0);

-- ----------------------------
-- Table structure for enterprise_resume
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_resume`;
CREATE TABLE `enterprise_resume`  (
                                      `enterprise_resume_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                      `eid` int(11) NOT NULL COMMENT '企业用户id',
                                      `resume_id` int(11) NOT NULL COMMENT '简历id',
                                      `score` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '简历评分',
                                      `gmt_create` datetime NOT NULL COMMENT '创建时间',
                                      `gmt_modified` datetime NOT NULL COMMENT '修改时间',
                                      `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
                                      `recruit_id` int(11) NOT NULL COMMENT '招聘id（外键）',
                                      PRIMARY KEY (`enterprise_resume_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of enterprise_resume
-- ----------------------------
INSERT INTO `enterprise_resume` VALUES (9, 1, 8, '67.0', '2021-12-27 23:12:52', '2021-12-27 23:12:52', 0, 1);

-- ----------------------------
-- Table structure for position
-- ----------------------------
DROP TABLE IF EXISTS `position`;
CREATE TABLE `position`  (
                             `pid` int(11) NOT NULL AUTO_INCREMENT COMMENT '岗位id',
                             `parent_id` int(11) NULL DEFAULT NULL COMMENT '父岗位',
                             `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位名称',
                             `gmt_create` datetime NOT NULL COMMENT '创建时间',
                             `gmt_modified` datetime NOT NULL COMMENT '修改时间',
                             `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
                             PRIMARY KEY (`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of position
-- ----------------------------
INSERT INTO `position` VALUES (1, 0, '后端开发', '2021-12-27 15:01:17', '2021-12-27 15:01:17', 0);
INSERT INTO `position` VALUES (2, 1, 'java工程师', '2021-12-27 15:01:17', '2021-12-27 15:01:17', 0);

-- ----------------------------
-- Table structure for recruit
-- ----------------------------
DROP TABLE IF EXISTS `recruit`;
CREATE TABLE `recruit`  (
                            `recruit_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '招聘id',
                            `eid` int(11) NOT NULL COMMENT '企业用户id（外键）',
                            `post` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '二级岗位',
                            `position` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '招聘岗位',
                            `workplace` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工作地点',
                            `numbers_limit` int(11) NULL DEFAULT NULL COMMENT '招聘人数',
                            `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '电子邮箱',
                            `telephone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系电话',
                            `major_require` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '专业要求',
                            `salary` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '薪资',
                            `education_require` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学历要求',
                            `contacts` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人',
                            `experience_require` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '经验要求',
                            `age_require` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年龄要求',
                            `gender_require` int(11) NULL DEFAULT NULL COMMENT '性别要求0男，1女,2不限',
                            `gmt_create` datetime NOT NULL COMMENT '创建时间',
                            `gmt_modified` datetime NOT NULL COMMENT '修改时间',
                            `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
                            PRIMARY KEY (`recruit_id`) USING BTREE,
                            INDEX `recruit_eid`(`eid`) USING BTREE,
                            CONSTRAINT `recruit_eid` FOREIGN KEY (`eid`) REFERENCES `enterprise` (`eid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of recruit
-- ----------------------------
INSERT INTO `recruit` VALUES (1, 1, 'java工程师', '后端开发', '黑龙江省大庆市', 20, '673354892@qq.com', '15776545893', '软件工程', '5000', '本科以上', '郜先生', '3-5年', '30岁左右', 0, '2021-12-27 15:01:17', '2021-12-27 15:01:17', 0);

-- ----------------------------
-- Table structure for resume
-- ----------------------------
DROP TABLE IF EXISTS `resume`;
CREATE TABLE `resume`  (
                           `resume_id` int(11) NOT NULL COMMENT '自增简历id(主键)',
                           `salary_require` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '薪资要求',
                           `aim_job_position` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '求职岗位',
                           `gmt_create` datetime NOT NULL COMMENT '创建时间',
                           `gmt_modified` datetime NOT NULL COMMENT '修改时间',
                           `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
                           `job_category` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位类别',
                           `send` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否投递1（true）投递，0（false）未投递',
                           `graduated` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '毕业院校',
                           `major` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '专业',
                           `personal_introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '个人介绍',
                           `desired_workplace` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '期望工作地点',
                           `experience` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工作经历',
                           `habitation` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '居住地',
                           `skill` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '技术特长',
                           PRIMARY KEY (`resume_id`) USING BTREE,
                           CONSTRAINT `resume_id` FOREIGN KEY (`resume_id`) REFERENCES `resume_base_info` (`resume_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of resume
-- ----------------------------
INSERT INTO `resume` VALUES (8, '6000', '后端', '2021-12-27 22:51:00', '2021-12-27 23:12:52', 0, '后端开发', 1, '哈尔滨理工学院', '软件工程', '喜欢读书', '黑龙江', '3年工作经历', '黑龙江大庆', '下棋');

-- ----------------------------
-- Table structure for resume_base_info
-- ----------------------------
DROP TABLE IF EXISTS `resume_base_info`;
CREATE TABLE `resume_base_info`  (
                                     `resume_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '简历id',
                                     `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '姓名',
                                     `gender` int(11) NOT NULL COMMENT '性别0男，1女',
                                     `birthday` datetime NULL DEFAULT NULL COMMENT '出生日期',
                                     `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
                                     `marry` int(11) NULL DEFAULT NULL COMMENT '0未婚，1已婚',
                                     `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '邮箱',
                                     `gmt_create` datetime NOT NULL COMMENT '创建时间',
                                     `gmt_modified` datetime NOT NULL COMMENT '修改时间',
                                     `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
                                     PRIMARY KEY (`resume_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of resume_base_info
-- ----------------------------
INSERT INTO `resume_base_info` VALUES (8, '郜宇博', 0, '2001-12-27 06:39:38', '15776545893', 0, '673354892@qq.com', '2021-12-27 22:49:27', '2021-12-27 22:49:27', 0);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
                         `uid` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
                         `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
                         `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
                         `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机号',
                         `gmt_create` datetime NOT NULL COMMENT '创建时间',
                         `is_deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
                         `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
                         `gmt_modified` datetime NOT NULL COMMENT '修改时间',
                         `resume_id` int(11) NOT NULL DEFAULT 0 COMMENT '简历id',
                         PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (3, 'username', 'password', 'telephone', '2021-12-20 13:00:23', 0, 'email', '2021-12-27 22:49:27', 8);

SET FOREIGN_KEY_CHECKS = 1;
