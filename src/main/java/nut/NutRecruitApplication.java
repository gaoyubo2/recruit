package nut;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 郜宇博
 */
@SpringBootApplication
public class NutRecruitApplication {

    public static void main(String[] args) {
        SpringApplication.run(NutRecruitApplication.class, args);
    }

}
