package nut.service;

import nut.entity.Position;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 郜宇博
 * @since 2021-12-20
 */
public interface PositionService extends IService<Position> {

}
