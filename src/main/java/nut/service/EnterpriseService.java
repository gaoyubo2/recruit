package nut.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import nut.entity.Enterprise;
import com.baomidou.mybatisplus.extension.service.IService;
import nut.entity.EnterpriseDisplay;
import nut.entity.Recruit;
import nut.entity.vo.ResumeInfoVo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author 郜宇博
 * @since 2021-12-19
 */
public interface EnterpriseService extends IService<Enterprise> {

    String login(Enterprise enterpriseUser);

    void register(Enterprise enterpriseUser);

    Enterprise getUserByToken(HttpServletRequest request);
    /**
     * 添加公司基本信息
     * @param enterpriseDisplay 公司基本信息
     */
    void addDisplayInfo(EnterpriseDisplay enterpriseDisplay,int eid);

    /**
     * 分页获取本公司简历
     * @param page 分页信息
     * @param eid 公司id
     * @return resumeList
     */
    IPage<ResumeInfoVo> getResumeList(Page<ResumeInfoVo> page, int eid);

    /**
     * 获取所有简历信息
     * @param resumeId 简历id
     * @return resumeInfo
     */
    Map<String,Object> getResumeAllInfo(int resumeId);

    /**
     * 获取公司基本信息
     * @param eid 企业id
     * @return 企业id
     */
    EnterpriseDisplay getDisplayInfo(int eid);

    /**
     * 根据eid获取所有招聘
     * @param eid eid
     * @return Recruits
     */
    List<Recruit> getRecruitsByEid(int eid);
}
