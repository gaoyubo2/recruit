package nut.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import nut.entity.Recruit;
import com.baomidou.mybatisplus.extension.service.IService;
import nut.entity.vo.PositionVO;

import java.util.List;

/**
 * @author 郜宇博
 * @since 2021-12-19
 */
public interface RecruitService extends IService<Recruit> {

    /**
     * 添加招聘信息
     */
    void addRecruit(Recruit recruit,int eid);

    /**
     * 分页获取所有招聘信息
     * @param page 页码信息（当前页，每页数量）
     * @param eid 企业id
     * @return 分页信息
     */
    IPage<Recruit> pageListRecruits(Page<Recruit> page, int eid);

    /**
     * 按照招聘岗位分类获取招聘
     * @return 招聘集合
     */
    List<PositionVO> getRecruitsByLevel();

    /**
     * 根据二级岗位分类获取招聘信息
     * @param page 页码信息（当前页，每页数量）
     * @param post 二级分类
     * @return 分页信息
     */
    IPage<Recruit> getPageRecruitsByPost(Page<Recruit> page, String post);

    IPage<Recruit> pageListRecruits(Page<Recruit> page);
}
