package nut.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import nut.common.handler.exceptionhandler.MyException;
import nut.entity.Enterprise;
import nut.entity.EnterpriseDisplay;
import nut.entity.Position;
import nut.entity.Recruit;
import nut.entity.vo.ChildPositionVO;
import nut.entity.vo.PositionVO;
import nut.mapper.EnterpriseDisplayMapper;
import nut.mapper.EnterpriseMapper;
import nut.mapper.PositionMapper;
import nut.mapper.RecruitMapper;
import nut.service.RecruitService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * @author 郜宇博
 * @since 2021-12-19
 */
@Service
public class RecruitServiceImpl extends ServiceImpl<RecruitMapper, Recruit> implements RecruitService {
    private final EnterpriseMapper enterpriseMapper;
    private final PositionMapper positionMapper;
    private final EnterpriseDisplayMapper enterpriseDisplayMapper;
    private final RecruitMapper recruitMapper;

    public RecruitServiceImpl(EnterpriseMapper enterpriseMapper,PositionMapper positionMapper,EnterpriseDisplayMapper enterpriseDisplayMapper,RecruitMapper recruitMapper) {
        this.enterpriseMapper = enterpriseMapper;
        this.positionMapper = positionMapper;
        this.enterpriseDisplayMapper = enterpriseDisplayMapper;
        this.recruitMapper = recruitMapper;
    }
    /**
     * 添加招聘信息
     */
    @Override
    public void addRecruit(Recruit recruit,int eid) {
        //是否存在该公司
        //提高效率，只需要查询是否存在，不需要查出具体对象
        int count = enterpriseMapper.selectCount(new QueryWrapper<Enterprise>().eq("eid", eid));
        if (count == 0){
            throw new MyException(20001,"公司不存在，eid错误");
        }
        String position = recruit.getPosition();
        String post = recruit.getPost();
        String email = recruit.getEmail();
        String telephone = recruit.getTelephone();
        String salary = recruit.getSalary();
        boolean hasEmpty;
        hasEmpty = (StringUtils.isEmpty(post)) || (StringUtils.isEmpty(position)) ||(StringUtils.isEmpty(email)) ||(StringUtils.isEmpty(telephone)) ||(StringUtils.isEmpty(salary));
        //非空校验
        if (hasEmpty){
            throw new MyException(20001,"必要招聘信息不完整");
        }else {
            //新建岗位
            addPosition(position,post);
            recruit.setEid(eid);
            baseMapper.insert(recruit);
        }
    }
    /**
     * 新建岗位
     * @param position 父岗位名称
     * @param post 子岗位名称
     */
    private void addPosition(String position, String post) {
        //查找该父岗位
        Position parentPosition = positionMapper.selectOne(new QueryWrapper<Position>().eq("title", position));
        //查找子岗位
        Position childPosition = positionMapper.selectOne(new QueryWrapper<Position>().eq("title", post));
        if (parentPosition == null){
            //新建父岗位
            parentPosition = new Position();
            parentPosition.setParentId(0);
            parentPosition.setTitle(position);
            positionMapper.insert(parentPosition);
        }
        if (childPosition == null){
            //获取父岗位的pid
            int parentId = parentPosition.getPid();
            //新建子岗位
            childPosition = new Position();
            childPosition.setParentId(parentId);
            childPosition.setTitle(post);
            positionMapper.insert(childPosition);
        }
    }
    /**
     * 分页获取所有招聘信息
     * @param page 页码信息（当前页，每页数量）
     * @param eid 企业id
     * @return pageList
     */
    @Override
    public IPage<Recruit> pageListRecruits(Page<Recruit> page, int eid) {
        //是否存在该公司
        //提高效率，只需要查询是否存在，不需要查出具体对象
        int count = enterpriseMapper.selectCount(new QueryWrapper<Enterprise>().eq("eid", eid));
        if (count == 0){
            throw new MyException(20001,"公司不存在，eid错误");
        }
        QueryWrapper<Recruit> wrapper = new QueryWrapper<>();
        wrapper.eq("eid",eid);
        return getRecruitPage(page, wrapper);
    }

    /**
     * 按照招聘岗位分类获取招聘
     * @return 招聘集合
     */
    @Override
    public List<PositionVO> getRecruitsByLevel() {
        //获取所有一级岗位
        List<Position> parentPositionList = positionMapper.selectList(new QueryWrapper<Position>().eq("parent_id", 0));
        //获取所有二级岗位
        List<Position> childPositionList = positionMapper.selectList(new QueryWrapper<Position>().ne("parent_id", 0));
        //将一级岗位封装到结果集中
        //结果集
        ArrayList<PositionVO> finalList = new ArrayList<>();
        for(Position parentPosition :parentPositionList){
            //将parentPosition更改为PositionVO对象
            PositionVO positionVO = new PositionVO();
            BeanUtils.copyProperties(parentPosition,positionVO);
            //OneSubject对象存入结果集
            finalList.add(positionVO);
            //将该一级分类对应的二级分类筛选
            //二级分类结果集
            ArrayList<ChildPositionVO> finalChildList = new ArrayList<>();
            for (Position childPosition: childPositionList){
                //属于该一级分类
                if (childPosition.getParentId().equals(parentPosition.getPid())){
                    //将该对象转化为二级分类对象，然后放入二级分类结果集中
                    ChildPositionVO childPositionVO = new ChildPositionVO();
                    BeanUtils.copyProperties(childPosition,childPositionVO);
                    //放入二级分类结果集中
                    finalChildList.add(childPositionVO);
                }
            }
            //将二级分类结果集添加到该一级分类中
            positionVO.setChildPositionList(finalChildList);
        }
        return finalList;
    }

    /**
     * 根据二级岗位分类获取招聘信息
     * @param page 页码信息（当前页，每页数量）
     * @param post 二级分类
     * @return 分页信息
     */
    @Override
    public IPage<Recruit> getPageRecruitsByPost(Page<Recruit> page, String post) {
        QueryWrapper<Recruit> wrapper = new QueryWrapper<>();
        wrapper.like("post",post);
        return getRecruitPage(page, wrapper);
    }

    /**
     * 帮助方法 根据wrapper不同获取不同分页简历
     * @param page 分页信息
     * @param wrapper 查询条件
     * @return pageList
     */
    private IPage<Recruit> getRecruitPage(Page<Recruit> page, QueryWrapper<Recruit> wrapper) {
        IPage<Recruit> pageList;
        try {
            pageList = recruitMapper.selectPage(page, wrapper);
            List<Recruit> recruits = pageList.getRecords();
            //为简历的企业名赋值
            for (Recruit recruit : recruits){
                //获取企业名称
                EnterpriseDisplay enterprise = enterpriseDisplayMapper.selectById(recruit.getEid());
                recruit.setEnterpriseName(enterprise.getName());
            }
        } catch (Exception e) {
            throw  new MyException(20001,"获取分页招聘信息失败");
        }
        return pageList;
    }

    /**
     * 获取所有招聘信息
     * @param page 分页信息
     * @return recruitPage
     */
    @Override
    public IPage<Recruit> pageListRecruits(Page<Recruit> page) {
        IPage<Recruit> recruitPage = recruitMapper.selectPage(page, null);
        List<Recruit> recruits = recruitPage.getRecords();
        //为简历的企业名赋值
        for (Recruit recruit : recruits){
            //获取企业名称
            EnterpriseDisplay enterprise = enterpriseDisplayMapper.selectById(recruit.getEid());
            recruit.setEnterpriseName(enterprise.getName());
        }
        return recruitPage;
    }
}
