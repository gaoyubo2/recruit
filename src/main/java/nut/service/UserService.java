package nut.service;

import nut.entity.Resume;
import nut.entity.ResumeBaseInfo;
import nut.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import nut.entity.vo.RecruitWithScoreVo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author 郜宇博
 * @since 2021-12-19
 */
public interface UserService extends IService<User> {

    /**
     * 登录
     * @param user 用户信息
     * @return token
     */
    String login(User user);

    /**
     * 注册
     * @param user 用户信息
     */
    void register(User user);

    /**
     * 获取用户信息
     * @param request 请求
     * @return 用户信息
     */
    User getUserByToken(HttpServletRequest request);

    /**
     * 添加简历基本信息
     * @param resumeBaseInfo 简历信息
     * @return 是否添加成功
     */
    int addResumeBaseInfo(ResumeBaseInfo resumeBaseInfo,int uid);

    /**
     * 添加简历关键信息
     * @param resumeCoreInfo 简历关键信息
     * @param resumeId 简历id
     * @return 是否添加成功
     */
    boolean addResumeCoreInfo(Resume resumeCoreInfo,int resumeId);

    /**
     * 投递简历
     * @param recruitId 招聘id
     * @param resumeId 简历id
     * @param uid 用户id
     * @return flag
     */
    boolean submitResume(int recruitId, int resumeId, int uid);


    /**
     * 获取所有招聘，并匹配自己的简历打分
     * @param resumeId 简历id
     * @return recruitList
     */
    List<RecruitWithScoreVo> getRecruitsWithScore(int resumeId);

    /**
     * 更新简历基础信息
     * @param resumeBaseInfo 简历基础信息
     * @param resumeId 简历id
     * @return flag
     */
    boolean updateResumeBaseInfo(ResumeBaseInfo resumeBaseInfo, int resumeId);
    /**
     * 更新简历核心信息
     * @param resumeCoreInfo 简历核心信息
     * @param resumeId 简历id
     * @return flag
     */
    boolean updateResumeCoreInfo(Resume resumeCoreInfo, int resumeId);
    /**
     * 打分
     * @return 分数
     */
    String getResumeScore(int recruitId, int resumeId);
}
