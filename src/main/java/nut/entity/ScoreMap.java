package nut.entity;

import io.swagger.models.auth.In;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: 郜宇博
 * @Date: 2021/12/20 18:59
 */
@Component
@ConfigurationProperties(prefix = "score")
@Data
public class ScoreMap {
    //private Map<String,String> scoreMap = new HashMap<>();
    //岗位
    private int position;
    //工作地点
    private int workplace;
    //专业
    private int major;
    //薪资
    private int salary;
    //学历
    private int education;
    //年龄
    private int age;
    //性别0男 1女 2不限
    private Integer gender;
    //211 985院校
    private String schoolList;

}
