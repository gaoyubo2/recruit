package nut.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author 郜宇博
 * @since 2021-12-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="EnterpriseDisplay对象", description="EnterpriseDisplay对象")
public class EnterpriseDisplay implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "企业用户id（外键、主键）")
    @TableId(value = "eid", type = IdType.ID_WORKER)
    private Integer eid;

    @ApiModelProperty(value = "公司简介")
    private String description;

    @ApiModelProperty(value = "公司地址")
    private String address;

    @ApiModelProperty(value = "公司官网")
    private String website;

    @ApiModelProperty(value = "联系电话")
    private String telephone;

    @ApiModelProperty(value = "公司名称")
    private String name;

    @ApiModelProperty(value = "注册时间")
    private Date registrationTime;

    @ApiModelProperty(value = "注册资金")
    private String registeredCapital;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @TableLogic
    @ApiModelProperty(value = "逻辑删除 1（true）已删除， 0（false）未删除")
    private Boolean isDeleted;


}
