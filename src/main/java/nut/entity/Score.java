package nut.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * @Author: 郜宇博
 * @Date: 2021/12/20 19:39
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
public class Score {
    //岗位
    private int position;
    //工作地点
    private int workplace;
    //专业
    private int major;
    //薪资
    private int salary;
    //学历
    private int education;
    //年龄
    private int age;

    public double getSum(ScoreMap scoreMap){
        double positionPart = position * 0.1 * scoreMap.getPosition();
        double workplacePart = workplace * 0.1 * scoreMap.getWorkplace();
        double majorPart = major * 0.1 * scoreMap.getMajor();
        double salaryPart = salary * 0.1 * scoreMap.getSalary();
        double educationPart = education * 0.1 * scoreMap.getEducation();
        double agePart = age * 0.1 * scoreMap.getAge();
        double finalScore = positionPart+workplacePart+majorPart+salaryPart+educationPart+agePart;
        //满分5分
        finalScore /=20;
        return Math.round(finalScore * 10) / 10.0;
    }
}
