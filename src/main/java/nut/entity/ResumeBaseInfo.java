package nut.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author 郜宇博
 * @since 2021-12-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ResumeBaseInfo对象", description="ResumeBaseInfo对象")
public class ResumeBaseInfo implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "简历id")
    @TableId(value = "resume_id", type = IdType.AUTO)
    private Integer resumeId;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "性别0男，1女")
    private Integer gender;

    @ApiModelProperty(value = "出生日期")
    private Date birthday;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "0未婚，1已婚")
    private Integer marry;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @TableLogic
    @ApiModelProperty(value = "逻辑删除 1（true）已删除， 0（false）未删除")
    private Boolean isDeleted;


}
