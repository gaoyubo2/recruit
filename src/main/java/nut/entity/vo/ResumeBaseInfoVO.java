package nut.entity.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author: 郜宇博
 * @Date: 2021/12/29 11:56
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResumeBaseInfoVO {

    @ApiModelProperty(value = "简历id")
    private Integer resumeId;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "性别0男，1女")
    private Integer gender;

    @ApiModelProperty(value = "出生日期")
    private Date birthday;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "0未婚，1已婚")
    private Integer marry;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "上次修改时间")
    private Date updateTime;

}
