package nut.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: 郜宇博
 * @Date: 2021/12/20 21:33
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChildPositionVO {
    @ApiModelProperty(value = "岗位名称")
    private String title;
    @ApiModelProperty(value = "岗位id")
    private Integer pid;
}
