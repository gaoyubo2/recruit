package nut.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import nut.entity.Position;

import java.util.List;

/**
 * @Author: 郜宇博
 * @Date: 2021/12/20 21:23
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PositionVO {
    @ApiModelProperty(value = "二级岗位列表")
    private List<ChildPositionVO> childPositionList;
    @ApiModelProperty(value = "岗位名称")
    private String title;
    @ApiModelProperty(value = "岗位id")
    private Integer pid;
}
