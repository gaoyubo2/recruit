package nut.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: 郜宇博
 * @Date: 2021/12/27 21:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RecruitWithScoreVo {
    @ApiModelProperty(value = "招聘id")
    private Integer recruitId;

    @ApiModelProperty(value = "企业用户id")
    private Integer eid;

    @ApiModelProperty(value = "招聘岗位")
    private String position;

    @ApiModelProperty(value = "二级岗位")
    private String post;

    @ApiModelProperty(value = "工作地点")
    private String workplace;

    @ApiModelProperty(value = "招聘人数")
    private Integer numbersLimit;

    @ApiModelProperty(value = "电子邮箱")
    private String email;

    @ApiModelProperty(value = "联系电话")
    private String telephone;

    @ApiModelProperty(value = "专业要求")
    private String majorRequire;

    @ApiModelProperty(value = "薪资")
    private String salary;

    @ApiModelProperty(value = "学历要求")
    private String educationRequire;

    @ApiModelProperty(value = "联系人")
    private String contacts;

    @ApiModelProperty(value = "经验要求")
    private String experienceRequire;

    @ApiModelProperty(value = "年龄要求")
    private String ageRequire;

    @ApiModelProperty(value = "性别要求0男，1女,2不限")
    private Integer genderRequire;

    @ApiModelProperty(value = "公司名称")
    private String enterpriseName;

    @ApiModelProperty(value = "简历评分")
    private String score;

}
