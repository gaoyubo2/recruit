package nut.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author: 郜宇博
 * @Date: 2021/12/29 11:57
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResumeCoreInfoVO {
    @ApiModelProperty(value = "简历id(主键)")
    private Integer resumeId;

    @ApiModelProperty(value = "薪资要求")
    private String salaryRequire;

    @ApiModelProperty(value = "求职岗位")
    private String aimJobPosition;

    @ApiModelProperty(value = "岗位类别")
    private String jobCategory;

    @ApiModelProperty(value = "是否投递1（true）投递，0（false）未投递")
    private Integer send;

    @ApiModelProperty(value = "毕业院校")
    private String graduated;

    @ApiModelProperty(value = "专业")
    private String major;

    @ApiModelProperty(value = "个人介绍")
    private String personalIntroduction;

    @ApiModelProperty(value = "期望工作地点")
    private String desiredWorkplace;

    @ApiModelProperty(value = "工作经历")
    private String experience;

    @ApiModelProperty(value = "居住地")
    private String habitation;

    @ApiModelProperty(value = "技术特长")
    private String skill;

    @ApiModelProperty(value = "上次修改时间")
    private Date updateTime;
}
