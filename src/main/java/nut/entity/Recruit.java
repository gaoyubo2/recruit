package nut.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author 郜宇博
 * @since 2021-12-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Recruit对象", description="Recruit对象")
public class Recruit implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "招聘id")
    @TableId(value = "recruit_id", type = IdType.AUTO)
    private Integer recruitId;

    @ApiModelProperty(value = "企业用户id（外键）")
    private Integer eid;

    @ApiModelProperty(value = "招聘岗位")
    private String position;

    @ApiModelProperty(value = "二级岗位")
    private String post;

    @ApiModelProperty(value = "工作地点")
    private String workplace;

    @ApiModelProperty(value = "招聘人数")
    private Integer numbersLimit;

    @ApiModelProperty(value = "电子邮箱")
    private String email;

    @ApiModelProperty(value = "联系电话")
    private String telephone;

    @ApiModelProperty(value = "专业要求")
    private String majorRequire;

    @ApiModelProperty(value = "薪资")
    private String salary;

    @ApiModelProperty(value = "学历要求")
    private String educationRequire;

    @ApiModelProperty(value = "联系人")
    private String contacts;

    @ApiModelProperty(value = "经验要求")
    private String experienceRequire;

    @ApiModelProperty(value = "年龄要求")
    private String ageRequire;

    @ApiModelProperty(value = "性别要求0男，1女,2不限")
    private Integer genderRequire;

    @TableField(exist = false)
    @ApiModelProperty(value = "公司名称")
    private String enterpriseName;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @TableLogic
    @ApiModelProperty(value = "逻辑删除 1（true）已删除， 0（false）未删除")
    private Boolean isDeleted;


}
