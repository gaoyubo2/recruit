package nut.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import nut.common.handler.exceptionhandler.MyException;
import nut.common.utils.Result;
import nut.entity.Enterprise;
import nut.entity.EnterpriseDisplay;
import nut.entity.Recruit;
import nut.entity.vo.ResumeInfoVo;
import nut.service.EnterpriseService;
import nut.service.RecruitService;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author 郜宇博
 * @since 2021-12-19
 */
@RestController
@Api(tags = "企业用户Api")
@RequestMapping("/api")
public class EnterpriseController {
    private final EnterpriseService enterpriseService;
    private final RecruitService recruitService;

    public EnterpriseController(EnterpriseService enterpriseService,RecruitService recruitService) {
        this.enterpriseService = enterpriseService;
        this.recruitService = recruitService;
    }
    /**
     * 登录
     * @param enterpriseUser 企业用户登录信息
     * @return token
     */
    @ApiOperation("企业用户登录")
    @PostMapping("enterpriseLogin")
    public Result loginUser(@RequestBody Enterprise enterpriseUser){
        String token =  enterpriseService.login(enterpriseUser);
        return Result.ok().data("token",token);
    }
    /**
     *  注册
     *  @param enterpriseUser 企业用户注册输入的信息
     *  @return 无
     */
    @PostMapping("enterpriseRegister")
    @ApiOperation("企业用户注册")
    public Result registerUser(@RequestBody Enterprise enterpriseUser){
        enterpriseService.register(enterpriseUser);
        return Result.ok().message("注册成功");
    }
    /**
     * 通过token获取用户信息
     * @param request http请求
     * @return 用户信息
     */
    @ApiOperation("通过token获取企业用户信息")
    @GetMapping("enterpriseUserInfo")
    public Result getUserInfo(HttpServletRequest request){
        Enterprise enterpriseUser  = enterpriseService.getUserByToken(request);
        return Result.ok().data("enterpriseUserInfo",enterpriseUser);
    }

    /**
     * 添加企业基本信息
     * @param enterpriseDisplay 企业基本信息
     * @return wu
     */
    @ApiOperation("添加企业基本信息")
    @PostMapping("displayInfo")
    public Result addDisplayInfo(@RequestBody EnterpriseDisplay enterpriseDisplay,HttpServletRequest httpServletRequest){
        Enterprise userByToken = enterpriseService.getUserByToken(httpServletRequest);
        int eid = userByToken.getEid();
        enterpriseService.addDisplayInfo(enterpriseDisplay,eid);
        return Result.ok().message("添加公司基本信息成功");
    }
    /**
     * 获取企业基本信息
     * @return wu
     */
    @ApiOperation("获取企业基本信息")
    @GetMapping("displayInfo")
    public Result getDisplayInfo(HttpServletRequest httpServletRequest){
        Enterprise userByToken = enterpriseService.getUserByToken(httpServletRequest);
        int eid = userByToken.getEid();
        EnterpriseDisplay enterpriseDisplay = enterpriseService.getDisplayInfo(eid);
        return Result.ok().data("enterpriseDisplay",enterpriseDisplay);
    }

    /**
     * 企业用户添加招聘信息
     * @param recruit 招聘信息
     * @return 无
     */
    @ApiOperation("企业用户添加招聘信息")
    @PostMapping("recruit")
    public Result addRecruit(@RequestBody Recruit recruit,HttpServletRequest httpServletRequest){
        Enterprise userByToken = enterpriseService.getUserByToken(httpServletRequest);
        int eid = userByToken.getEid();
        recruitService.addRecruit(recruit,eid);
        return Result.ok().message("招聘发布成功");
    }

    /**
     * 分页获取投递的所有简历
     * @param current 当前页
     * @param limit 每页记录数
     * @return resumeList
     */
    @GetMapping("pageResumes/{current}/{limit}")
    @ApiOperation("分页获取投递的所有简历")
    public Result getResumeList(HttpServletRequest httpServletRequest,
                                @ApiParam(name = "current", value = "当前页码",required = true) @PathVariable Long current,
                                @ApiParam(name = "limit", value = "一页记录数",required = true) @PathVariable Long limit){
        Enterprise enterprise = enterpriseService.getUserByToken(httpServletRequest);
        int eid = enterprise.getEid();
        Page<ResumeInfoVo> page = new Page<>(current,limit);
        IPage<ResumeInfoVo> resumeList = enterpriseService.getResumeList(page, eid);
        return Result.ok().data("resumeList",resumeList);
    }

    /**
     * 根据简历Id获取简历信息
     * @param resumeId 简历Id
     * @return resumeAllInfo
     */
    @GetMapping("resume/{resumeId}")
    @ApiOperation("根据简历Id获取简历信息")
    public Result getResumeById( @ApiParam(name = "resumeId", value = "简历Id",required = true) @PathVariable int resumeId){
        if (StringUtils.isEmpty(resumeId)){
            throw new MyException(20001,"简历Id为空");
        }
        Map<String, Object> resumeAllInfo = enterpriseService.getResumeAllInfo(resumeId);
        return Result.ok().data(resumeAllInfo);
    }
    /**
     * 根据eid获取所有招聘
     * @return Recruits
     */
    @GetMapping("recruits")
    @ApiOperation("根据eid获取所有招聘")
    public Result getRecruits(HttpServletRequest httpServletRequest){
        Enterprise userByToken = enterpriseService.getUserByToken(httpServletRequest);
        int eid = userByToken.getEid();
        List<Recruit> recruitsList = enterpriseService.getRecruitsByEid(eid);
        return Result.ok().data("recruitsList",recruitsList);
    }
}

