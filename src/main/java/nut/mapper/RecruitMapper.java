package nut.mapper;

import nut.entity.Recruit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 郜宇博
 * @since 2021-12-19
 */
@Mapper
public interface RecruitMapper extends BaseMapper<Recruit> {

}
