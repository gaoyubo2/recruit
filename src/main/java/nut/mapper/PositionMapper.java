package nut.mapper;

import nut.entity.Position;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郜宇博
 * @since 2021-12-20
 */
@Mapper
public interface PositionMapper extends BaseMapper<Position> {

}
