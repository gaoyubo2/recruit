package nut.common.utils;

import lombok.Data;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @Author: 郜宇博
 * @Date: 2021/12/20 20:13
 */
public class AgeUtil {
    /**
     * 计算年龄
     * @param birthday 生日
     * @return
     */
    public static int getAge(String birthday){
        String ageStr =  birthday.split("-")[0];
        int age = Integer.parseInt(ageStr);
        //当前年
        String nowStr= LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String nowYear = nowStr.split("-")[0];
        int nowYearInt = Integer.parseInt(nowYear);
        return nowYearInt - age;
    }
    public static int getRequeireAge(String str){
        String[] strings = str.split("岁");
        int length = strings[0].length();
        return Integer.parseInt(strings[0].substring(length-2,length));
    }
}
