package nut.common.utils;

/**
 * @author 郜宇博
 * @since 2021-12-19
 */
public interface ResultCode {
    /**
     * 成功
     */
    Integer SUCCESS = 20000;
    /**
     * 失败
     */
    Integer ERROR = 20001;
    String NULL = "null";

}
